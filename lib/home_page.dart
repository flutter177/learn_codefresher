import 'package:flutter/material.dart';

class HomePage extends StatefulWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawer: Drawer(
        child: Column(
          // padding: EdgeInsets.zero,

          children: [
            UserAccountsDrawerHeader(
              accountEmail: Text('hoanghuuanh19991@gmail.com'),
              accountName: Text('Hoàng Hữu Mạnh'),
              currentAccountPicture: CircleAvatar(
                backgroundColor: Colors.transparent,
                backgroundImage: NetworkImage(
                    'https://cdn-img.thethao247.vn/origin_1920x0/storage/files/tranvutung/2022/07/22/psg-chot-gia-bat-ngo-ga-khong-lo-ngoai-hang-anh-sap-mo-hoi-don-neymar-161052.jpeg'),
              ),
            ),
            Expanded(
              child: SingleChildScrollView(
                child: Column(
                  children: [
                    ListTile(
                      leading: Icon(Icons.home),
                      title: Text('Home'),
                      onTap: () {
                        Navigator.of(context).pop();
                      },
                    ),
                    ListTile(
                      leading: Icon(Icons.settings),
                      title: Text('Settings'),
                      onTap: () {
                        Navigator.pushNamed(context, '/second');
                      },
                    ),
                    ListTile(
                      leading: Icon(Icons.contacts),
                      title: Text('Contact Us'),
                      onTap: () {},
                    ),
                    ListTile(
                      leading: Icon(Icons.home),
                      title: Text('Home'),
                      onTap: () {
                        Navigator.of(context).pop();
                      },
                    ),
                    ListTile(
                      leading: Icon(Icons.settings),
                      title: Text('Settings'),
                      onTap: () {},
                    ),
                    ListTile(
                      leading: Icon(Icons.contacts),
                      title: Text('Contact Us'),
                      onTap: () {},
                    ),
                    ListTile(
                      leading: Icon(Icons.home),
                      title: Text('Home'),
                      onTap: () {
                        Navigator.of(context).pop();
                      },
                    ),
                    ListTile(
                      leading: Icon(Icons.settings),
                      title: Text('Settings'),
                      onTap: () {},
                    ),
                    ListTile(
                      leading: Icon(Icons.contacts),
                      title: Text('Contact Us'),
                      onTap: () {},
                    ),
                  ],
                ),
              ),
            ),
            Container(
                margin: EdgeInsets.only(bottom: 20),
                child: Text('Copy right @2022')),
          ],
        ),
      ),
      appBar: AppBar(
        title: Text('Learn Code Fresher'),
      ),
      body: Center(
        child: Text('Hoang Huu Manh'),
      ),
    );
  }
}
